<?php

if (isset($_POST) & !empty($_POST)) {

    //include ("connect.php"); // connection from Another PHP Page
    $db = new PDO('mysql:host=localhost;dbname=arif;charset=utf8mb4', 'root', '');

    $name = $_POST['Name'];
    $email = $_POST['Email'];
    $id = $_POST['ID'];
    $cell = $_POST['Mobile'];
    $semester = $_POST['Semester'];
    $type = $_POST['participate'];

    $sql = "INSERT INTO `info`(`name`, `email`, `id`, `cell`, `semester`, `type`) VALUES ($name,$email, $id, $cell, $semester, $type";

   /* if ($conn->query($sql) === TRUE) {
        echo "<h2 style='color:whitesmoke; text-align: center'> You Are successfully Registered </h2>";
    } else {
       //echo "Error: " . $sql . "<br>" . $conn->error;
        echo "<h2 style='color:whitesmoke; text-align: center'> You Are Already Registered </h2>";
    }
}*/
?>

<html>
    <head>
        <title>Form Validation </title>
        <link rel="shortcut icon" href="logo.png" />
        <style>
            body{
                background-image: url("set.gif");
            }
            h2 {
                color: black;
                font-weight: bolder;
            }
            table {
                color: whitesmoke;
                font-weight: bolder;
            }
            fieldset{
                width: 30%;
                margin: 0px 440px;
                text-align: center;

            }
            .btn{
                background-color: #f4511e; /* Green */

                border-color:cornsilk;
                color: white;
                padding: 10px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                border-radius: 6px;
                width: 150px;
            }
            input{
                border-radius: 4px;
                font-weight: 500;
                font-size: large;
                width: 210px;
            }
            select{
                border-radius: 4px;
                font-weight: 500;
                font-size: medium;
                width: 210px;
            }
            img{
            ;
                width: 100px;
                height: 70px;
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
    <div align="center">
        <img src="logo.png" alt="logo" >
    </div>
    <fieldset>
        <legend>
            <h2> Event Registration </h2>
        </legend>

        <form name="myForm" method="post" onsubmit="return validate();">

            <table align="center"   cellspacing="6"  cellpadding="10"  border="1">

                <tr>
                    <td align="center">Name </td>
                    <td><input type="text"  name="Name" /></td>
                </tr>

                <tr>
                    <td align="center">Email </td>
                    <td><input type="text"name="Email" /></td>
                </tr>

                <tr>
                    <td align="center">Your ID </td>
                    <td><input type="text"  name="ID" /></td>
                </tr>
                <tr>
                    <td align="center">Mobile </td>
                    <td><input type="text"  name="Mobile" /></td>
                </tr>

                <tr>
                    <td align="center">Semester </td>
                    <td><input type="text"   name="Semester" /></td>
                </tr>
                <tr>
                    <td align="center">Participate </td>
                    <td>
                        <select name="participate">
                            <option value="" selected>Select One</option>
                            <option value="Dancer">As A Dancer</option>
                            <option value="Singer">As A Singer</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="center">
                        <input class="btn" type="submit" value="Submit" />
                    </td>
                </tr>

            </table>
        </form>
    </fieldset>
    </body>
</html>